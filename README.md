# Oracle数据库管理

### Oracle数据库创建与删除

### Oracle的实例管理和诊断日志

- 使用sqlplus

  ```shell
  # 登录
  sqlplus system/oracle@itpuxdb
          [用户名]/[密码]@数据库名
  # 查询表结构
  DESCRIBE departments
  # 查询上一条命令
  list  
  L
  ---------------------------------
  SAVE filename  # 将当前内容保存到文件
  GET filename   # 获取一个文件进来
  START filename # 运行一个文件
  @ filename     # 执行脚本
  EDIT filename  # 编辑文件
  SPOOL filename # 当前屏幕内容输出到文件
  EXIT           # 退出
  ```

  ### Oracle数据库的开启与关闭

![5.png](https://raw.gitcode.com/Oracle-2024/Oracle-2/attachment/uploads/48fbb1a1-92cf-4ac0-afdf-1e925cb4ee77/5.png '5.png')

- Oracle数据库的关闭模式

  ABORT；IMMEDIATE；TRANSACTIONAL；NORMAL

  | 关闭模式        （x:NO   /    o:YES） | A    | I    | T    | N    |
  | ------------------------------------- | ---- | ---- | ---- | ---- |
  | 允许新的接入                          | x    | x    | x    | x    |
  | 等待直到当前的会话都结束              | x    | x    | x    | o    |
  | 等待直到当前的事务都结束              | x    | x    | o    | o    |
  | 强制执行checkpoint，并关闭所有文件    | x    | o    | o    | o    |




### Oracle参数文件管理

- 启动数据库时，调用参数文件（xxx.ora）来启动数据库实例。

- 数据库的设置/配置/优化/调整都是由参数文件控制的。

- 参数文件分为以下两类：

  - 静态初始化参数文件 PFILE（9i之前）
    - PFILE initSID.ora是一个编辑器修改的文件
    - 手动对该文件进行修改
    - 下次数据库启动时修改才能生效
    - 它的缺省路径是$ORACLE_HOME/dbs
  - 动态初始化参数文件 SPFILE（10g之后）
    - 是一个不用重新启动数据库就能将更改生效的二进制文件
    - 保存在ORACLE服务器端
    - 能通过ALTER SYSTEM 命令直接对参数进行修改
    - 能指定改变临时在内存中生效还是从现在开始永久生效
    - 修改的值能被删除，并重置到实例缺省的值。

- 创建SPFILE

  ```shell
  # SPFILE能使用CREATE SPFILE命令从initSID.ora中创建，并且在数据库开启之前或开启后都能执行。
  CREATE SPFILE FROM PFILE;
  ```

- 参数文件的启动顺序

  ```shell
  # 数据库启动时按照以下顺序查找参数文件，如查找不到就启动失败
  # 查找文件搜索方式是从缺省路径下开始
  spfile<sid>.ora  --->  spfile.ora  --->  init<sid>.ora
  ```

### Oracle控制文件管理

- 在数据库启动过程中，从mount状态开始，控制文件就处于使用状态中。
- Control File（控制文件）是一个较小的二进制文件（不会操作100M），描述数据库结构，包括：
  - 数据库建立日期
  - 数据库名
  - 数据库中所有的数据文件和日志文件的文件名和路径
  - 恢复数据库时所需的同步信息
  - 在打开和存取数据库时都要访问该文件
  - 一套控制文件只能连接一个database
  - 分散放置，至少1份，至多8份
  - 记录控制文件名和路径的参数为：CONTROL_FILES
  - 控制文件相当于数据库的大脑，所有操作都要通过控制文件
  - 控制文件每3秒会同步数据（心跳）；如增加表空间，则控制文件会立刻做出变更（结构上的变更和记录）

### Oracle重做日志文件管理

重做日志文件记录了数据所有的修改信息并提供一种数据库失败时的恢复机制

- 重做日志文件分组管理
- 一个Oracle数据库要求至少有两组重做日志文件
- 组中每个日志文件被称作一个组成员

下图为重做日志文件的结构，分group-1，group-2，group-3三个日志组，每个组有两个组成员，并且组成员分别放置在disk1和disk2中，做镜像备份。
![6.png](https://raw.gitcode.com/Oracle-2024/Oracle-2/attachment/uploads/7ab70956-7daa-4000-bc9b-d0502235259b/6.png '6.png')

- 重做日志文件循环使用

- 当一个重做日志文件写满时，LGWR进程将日志写入到下一个重做日志组

  - 这个叫做日志切换
  - 检查点执行动作被触发
  - 相关信息写入控制文件

- 重做日志的手工切换

  ```shell
  # 命令
  alter system switch logfile；
  ```

  

- 增加在线重做日志文件组

  ```shell
  # 增加日志组“group 3”，设置日志路径及每个日志文件的空间大小（日志的后缀可以自定义）
  alter database add logfile group 3 
  ('/oracle/oradata/fgedu/log3a.rdo',
  '/oracle/oradata/fgedu/log3ab.rdo')
  size 10M;
  ```

- 增加在线重做日志文件组的组成员

  ```shell
  # 分别在group 1，group 2， gourp 3三个组中添加log1c.rdo， log2c.rdo， log13c.rdo三个组成会员
  alter database add logfile member
  '/oracle/oradata/fgedu/log1c.rdo' to group 1,
  '/oracle/oradata/fgedu/log2c.rdo' to group 2,
  '/oracle/oradata/fgedu/log3c.rdo' to group 3;
  ```

- 删除在线重做日志文件组的成员

  ```shell
  # 删除日志组成员为/oracle/oradata/fgedu/log3c.rdo
  alter database drop logfile member 
  '/oracle/oradata/fgedu/log3c.rdo';
  ```

- 删除在线重做日志文件组

  ```shell
  # 删除日志组group 3
  alter database drop logfile group 3;
  ```

- 获取日志组及其成员信息

  ```
  视图： V$LOG
  视图： V$LOGFILE
  ```

### Oracle归档日志介绍

- 在线重做日志文件发生切换后，就需要被归档（如不归挡，再写入时会被清空）
- 归档在线重做日志文件的两种优势：恢复 ；备份
- 缺省数据库以非归档模式创建

### Oracle表空间和数据文件管理
- 存储结构
![7.png](https://raw.gitcode.com/Oracle-2024/Oracle-2/attachment/uploads/36ba1ca2-8d8f-4a51-a745-750d6b9d23ff/7.png '7.png')

- 表空间属性分类

  - 系统表空间
    - 随着数据库创建被创建
    - 包含数据字典信息
    - 包含系统回滚段

  - 非系统表空间
    - 不同的segments分开存放（如：回滚段；临时段；应用数据）
    - 控制分配给用户对象的空间容量

- 表空间管理分类

  - 本地管理的表空间（9i之后）

    - 自由扩展信息被记录在本身的位图中
    - 位图中的每一位都对应一个数据块或一组数据块
    - 位图中的标志位显示数据块使用或未使用状态信息

    ```shell
    # 本地管理表空间的特点
    1- 简化了数据字典中的内容
    2- 空间分配和回收不产生回滚信息
    3- 没有接合临近extents的要求
    # 本地表空间如何创建
    create tablespace fgedu 
    datafile '/oracle/oradata/fgedu.dbf' size 500M
    extent management local;
    ```

    

  - 字典管理的表空间（9i之前）

    - 缺省的表空间管理方式
    - 自由扩展信息被记录在数据字典中

    ```shell
    # 字典管理表空间的特点
    1- Extents在数据字典中管理
    2- 每个存储在表空间的segments都可以有不同的存储参数设置
    3- 有邻近接合extents的要求
    # 字典管理表空间如何创建
    create tablespace fgedu
    datafile '/oracle/oradata/fgedu.dbf' size 500M
    extent management dictionary
    default storage (initial 1M next 1M );
    ```

- 回滚段表空间

  - 用来存储回滚段信息
  - 不能包含其他对象
  - extents本地管理
  - 在创建时仅仅能使用datafile and extent anagement 条件

  ```shell
  create undo tablesapce undol
  datafile '/oracle/oradata/undo01.dbf' size 40M;
  ```

- 临时表空间

  - 用来做排序操作
  - 不能包含固定的对象
  - 最好使用本地表空间管理

  ```
  create temporary tablespace temp
  tempfile '/oracle/oradata/temp01.dbf' size 500M
  extent management local;
  ```

- 表空间删除

  - 从数据字典中删除表空间信息
  - 通过and datafiles条件可在操作系统上删除表空间的数据文件

  ```shell
  drop tablespace fgedu
  including contents and datafiles;
  ```

- 改变表空间大小（下线状态，目标文件必须存在）

  - 增加数据文件
  - 改变数据文件大小
    - 自动
    - 手动

  ```
  # 改变数据文件大小
  alter database datafile '/fgedu/oradata/app_data_02.dbf'
  resize 200M;
  # 增加数据文件
  alter tablespace userdata 
  rename
  datafile '/u01/oradata/userdata01.dbf'
  to '/u02/oradata/userdata01.dbf';
  ```

- 获取表空间信息

  - 表空间信息视图

    ```
    DBA_TABLESPACE
    V$TABLESPACE
    ```

  - 数据文件信息视图

    ```
    DBA_DATA_FILES
    V$DATAFILE
    ```

  - 临时文件信息视图

    ```
    DBA_TEMP_FILES
    V$TEMPFILE
    ```

### Oracle用户权限管理

**数据模式scheman**

- 模式是一种命名的对象集合
- 一个用户被创建，对应的模式就被创建
- 一个用户仅仅对应一个模式
- 用户名其实就等同于模式名

**创建用户所涉及的内容**

| 内容                                   |
| -------------------------------------- |
| 表（table）                            |
| 视图（view）                           |
| 索引（index）                          |
| 序列号（sequence）                     |
| 存储过程和函数（procedures&functions） |
| 触发器（triggers）                     |
| 同义词（synonym）                      |
| 包（package）                          |
| 数据库连接（database link）            |
| 快照（snapshot）                       |

- 确定用户需要将对象存储在哪个表空间
- 决定每个表空间中该用户的使用限额
- 指派缺省表空间和临时表空间
- 开始创建一个用户
- 赋予权限和角色给用户

**具体指令**

```shell
# 创建用户
create user itpux # 创建用户名为itpux
identified by itpux  # 定义它的密码
default tablespace data # 指派缺省表空间
temporary tablespace temp; # 指定临时表空间
------------------------------------------
# 删除用户
drop user itpux;
------------------------------------------
# 使用cascade条件删除用户及其用户中包含的所有对象
drop user itpux cascade;
------------------------------------------
# 当前正连接到oracle服务器的用户不能被删除
------------------------------------------
------------------------------------------
# 获取用户相关信息
视图：DBA_USERS
视图：DBA_TS_QUOTAS
```

**oracle系统权限**

- 有100多种
- any关键字是指用户在任何模式下有具有
- grant命令用于授予权限
- revoke命令收回权限

```shell
# 授权系统权限
grant create session to itpux;
grant create session to itpux with admin option;
# 授权系统对象
grant execute on dbms_output to itpux;
grant update on fgedu.yg to itpux with grant option;
# 回收对象权限
revoke select on fged.yg from itpux;
```

**查看权限信息**

查询以下数据字典视图

- DBA_SYS_PRIVS
- SESSION_PRIVS
- DBA_TAB_PRIVS
- DBA_COL_PRIVS

### Oracle监听网络管理

- 配置文件：
  - Listener.ora
  - Tnsnames.ora
- 配置图形命令：netca
- 启动/关闭/查看命令：lsnrctl start/stop/status
- 通过oracle client配置之tnsname.ora连接数据库

### Oracle EM管理工具

EM管理工具为集中式管理控制台，以下是EM管理工具的启动关闭方式：

- Oracle 11g：

  emctl start dbconsole

  emctl stop dbconsole

  https://192.168.1.61:1158/em/

- Oracle 12c:

  exec dbms_xdb_config.sethttpsport(5500);

  exec dbms_xdb_config.sethttpsport(5501);

  https://192.168.1.61:5500/em/

Oracle 10c客户端EM控制台兼容8以上所有版本，可使用10c的EM控制台统一管理。

（修改Oracle配置文件指定版本）

### Oracle备份和恢复基础

至少要保留两份备份拷贝，一份用于在线恢复，另一份则保留在离线环境中，离线拷贝可用作恢复的最后手段。

- 完整备份与增量备份（差异增量与积累增量）

  - 完整备份：一个或多个数据文件的完整备份，包括从备份开始的所有级的数据块。
  - 增量备份：包含从最近一次备份以来被修改过或增加过的数据块，增量备份又分为0级增量备份/1级差异增量备份/1级积累增量备份/增量备份支持归档和非归档模式，而且只能使用rman的时候才可以实现增量备份。
    - 差异增量：是备份上级及同级备份以来所有变化的数据块，差异增量是默认增量备份
    - 累计增量：是备份上级备份以来所有变化的数据块。

- 完全备份与非完全备份

  - 完全备份：包括所有数据文件/控制文件/参数文件/密码文件/在线重做日志文件/归档日志文件。
  - 非完全备份：比如备份一个或多个数据文件/一个或多个表空间等，非完全备份只限于在归档模式下才有效。

- 脱机备份于联机备份

  - 脱机备份：是在数据库关闭的时候发生的备份，又叫冷备份；是在一致性关闭数据库后，控制文件scn与数据文件头部的scn一致。
  - 联机备份：实在数据库使用的情况下发生的备份，又称为非一致性备份或者热备份；连接备份一个数据文件scn与控制文件不一致。联机备份可以是全备，也可以是部分备份，必须在归档模式下才可以完成。

- 物理备份与逻辑备份

  - 物理备份：是所有物理文件的一个副本，比如数据文件/日志文件/控制文件/归档文件等，这个文件都存在本地磁盘或相应的存储上面，而物理备份包括冷备份（非归档模式）或热备份（归档模式）
  - 逻辑备份：是将表/索引/存储过程等，可以使用oracle exp / expdp导出来的二进制文件，后面再通过oracle的imp/impdp导入到数据库；可以说扩及备份是对物理备份的一种补充吧，一般都是用在数据迁移上面。

- oracle备份与恢复中涉及的文件

  - 数据文件
  - 控制文件
  - 重做日志文件
  - 参数文件
  - 归档日志文件

- oracle的还原和恢复

  苏剧的恢复策略是使用最近一次备份来实现数据库的还原，然后使用归档日志和联机日志将数据库恢复搭配最新或特定的状态

  - 还原：是从最近的备份文件中查找所需要的内容，并将其拷回原来的位置的过程称为还原。可以基本数据库/表空间/数据文件/控制文件/参数文件进行还原。
  - 恢复：在还原的基础上，使用归档日志文件和联机日志文件将数据库刷新到最新的scn，使数据库保持一致性。

- oracle备份恢复的工具

  - imp/impdp       exp/expdp

    进行逻辑备份恢复，可以跨平台迁移，支持全库/用户/表级的备份与恢复，特别是在oracle 10g以后，expdp与impdp备份速度比较快，支持多并发。

  - rman

    物理备份恢复，支持命令行及图形接口，可以通过第三方软件（nbu/legato/tsm等）及磁带机进行备份，这个工具的功能非常强大

### Oracle数据库日常维护检查/故障与性能分析

- 日常维护检查内容
  - 保证数据库正常运行
  - 数据库日常备份检查
  - 定期查看alert_SID.log，发现现有的问题
  - 定期rebuild索引
  - 定期analyze tables和indexs
  - 定期检查数据库的空间使用情况
  - 定期检查Unix空间使用情况
  - 定期做数据库的恢复测试
  - 及时发现并解决数据库出现的问题
- 检查数据库的性能/空间/相关资源
  - 性能
  - 空间使用情况/空间增长情况
  - 定期analyze表和索引（analyze：用于分析表结构和数据的命令）
  - 定期rebuild索引（rebuild：重建命令，用于重建索引）

**scn：是一个用于表示数据库内部事务顺序的唯一标识符，scn用于跟踪和标识数据库中的每个更改操作，包括数据插入，更新和删除。**

- 数据库空间的规划和管理
  - 检查和积累主机的CPU/Memory/IO的使用发展情况
  - 检查和积累磁盘阵列的空间使用情况
  - 检查和积累Oracle系统load大小和load Profile
  - 判断什么时候需要添置新设备，给出合理的依据
  - 统计数据库里面重要对象的增长：记录数/使用空间

- 定期检查alert日志
  - 可以使用cat/more/vi等操作系统命令查看
  - 日志文件定期清理/备份
  - 发现有ORA-开头的错误信息，及时分析，相关详细错误信息在：$ORACLE_BASE/damin/SID名称/bdump中

​         

- oracle故障诊断方法：
![8.png](https://raw.gitcode.com/Oracle-2024/Oracle-2/attachment/uploads/5260573a-73d2-4c16-beef-634e96fa4d89/8.png '8.png')


### Oracle数据库高可用

- Oracle架构

  - Single Node（单机）

  - HA（Highly Available/高可用）

    - 一台主机宕机，另一台主机将接管磁盘；同时根据所定义的服务启动相应的进程，完成切换工作。
    - 允许系统在使用的同时在线更换CPU/内存/硬盘等硬件，保证系统7*24小时不间断工作

  - DataGuard（主备）

    - 通过主库生成一个备库
    - 通过网络传输归档日志

  - RAC（多节点）

    

